import requests
import os.path

def getKey():
    if not os.path.exists("data.info"):
        f = open("data.info", 'w+')
        f.write("apiKey = ")
        print("Generated data.info file, write there your SteamAPI key and restart library")
        f.close()
        exit()
    else:
        f = open("data.info", 'r')
        key = f.read()
        key = key.split(" = ")
        key = key[1]
        return key

def getSteamId64(userName):
    url = "http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/"
    params = dict(
    key = getKey(),
    vanityurl = userName,
    format='json')
    resp = requests.get(url=url, params=params)
    data = resp.json()
    return data['response']['steamid']
    

class App():
    def __init__(self, AppId):
        self.AppId = AppId
    def checkSuccess(self, result):
        return result[self.AppId]["success"]
    def getData(self):
        url = 'https://store.steampowered.com/api/appdetails/'
        params = dict(
        appids = self.AppId,
        format = 'json')
        resp = requests.get(url=url, params=params)
        data = resp.json()
        return data
    def metacriticScore(self):
        data = self.getData()
        if self.checkSuccess(data):
            score = -1
            if "metacritic" in data[self.AppId]["data"]:
                score = int(data[self.AppId]["data"]["metacritic"]["score"])
            return score
        else:
            return None
    def controllerSup(self):
        data = self.getData()
        if self.checkSuccess(data):
            if "controller_support" in data[self.AppId]["data"]:
                return data[self.AppId]["data"]["controller_support"]
            else:
                return "Unknown"
        else:
            return None
    def isFree(self):
        data = self.getData()
        if self.checkSuccess(data):
            return data[self.AppId]["data"]["is_free"]
        else:
            return None
class User():
    def __init__(self, UserId):
        UserId = UserId.split("/")
        UserId = UserId[len(UserId)-1]
        self.UserId = getSteamId64(UserId)
        self.games = []
        url = 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/'
        params = dict(
        key = getKey(),
        steamid = self.UserId,
        format='json')
        resp = requests.get(url=url, params=params)
        data = resp.json()
        lst = data["response"]["games"]
        for i in range(len(lst)):
            self.games.append(lst[i]["appid"])
